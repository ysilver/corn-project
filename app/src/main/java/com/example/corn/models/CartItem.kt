package com.example.corn.models

data class CartItem(val productID:String,val itemDesc:String,val imageName:String,val qnt:String,val price:String,val comments:String)