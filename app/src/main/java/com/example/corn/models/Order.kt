package com.example.corn

import com.google.gson.annotations.SerializedName

data class Order (

    @SerializedName("address") val address :String,
    @SerializedName("comments") val comments :String,
    @SerializedName("items") val items :MutableList<Item>
)
data class Item (


    @SerializedName("product_id") val productid :Long,

    @SerializedName("quantity") val quantity :Long,
    @SerializedName("comments") val comments :String
)