package com.example.corn.helpers
import android.os.Build

inline fun isAboveMarsh(code: () -> Unit, other: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        code()
    } else {
        other()
    }
}