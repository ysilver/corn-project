package com.example.corn.adapters

import android.view.View
import com.example.corn.Network.Data
import java.text.FieldPosition

interface RecyclerCallback {

    interface HistoryCallback {
        fun onRowClicked(position:Int, view: View)
    }

}