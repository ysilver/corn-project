package com.example.corn.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.corn.R
import com.example.corn.models.CartItem

class CustomAdapter(private val dataSet: ArrayList<CartItem>) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.recycleview_row, viewGroup, false)
        )

    override fun getItemCount() = dataSet.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(dataSet[position])
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemDesc: TextView = view.findViewById(R.id.itemDesc)
        val imageView: ImageView = view.findViewById(R.id.cornImage)
        val qnt: TextView = view.findViewById(R.id.quantity)
        val price: TextView = view.findViewById(R.id.price)
        val comments: TextView = view.findViewById(R.id.comments)

        fun bind(cartItem: CartItem) {
            itemDesc.text = cartItem.itemDesc
            qnt.text = cartItem.qnt
            price.text = cartItem.price
            comments.text = cartItem.comments
            if (cartItem.imageName == "bolied") {
                imageView.setBackgroundResource(R.drawable.bolied)
            } else {
                imageView.setBackgroundResource(R.drawable.roasted)
            }
        }
    }
}