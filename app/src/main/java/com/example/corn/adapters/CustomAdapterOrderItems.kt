package com.example.corn.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.corn.Network.Data
import com.example.corn.Network.Order_items
import com.example.corn.R

class CustomAdapterOrderItems(private val dataSet: List<Order_items>, private val callback: RecyclerCallback.HistoryCallback) :
    RecyclerView.Adapter<CustomAdapterOrderItems.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.recycleview_row, viewGroup, false)
        )

    override fun getItemCount() = dataSet.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataSet[position], callback)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val qnt: TextView = itemView.findViewById(R.id.quantity)
        val image: ImageView = itemView.findViewById(R.id.cornImage)
        val itemDesc: TextView = itemView.findViewById(R.id.itemDesc)
        val price: TextView = itemView.findViewById(R.id.price)
        val comments: TextView = itemView.findViewById(R.id.comments)

        fun bind(data: Order_items, callback: RecyclerCallback.HistoryCallback) {
            itemDesc.text = data.name
            qnt.text = data.quantity.toString()
            price.text = data.charge.toString()
            comments.text = data.comments
            if (data.id == 1){
                image.setBackgroundResource(R.drawable.roasted)
            }
            else{
                image.setBackgroundColor(R.drawable.bolied)
            }
            itemView.setOnClickListener {
            //    callback.onRowClicked(data, it)
            }
        }
    }


}