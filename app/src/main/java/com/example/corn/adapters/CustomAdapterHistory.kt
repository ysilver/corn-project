package com.example.corn.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.corn.Network.Data
import com.example.corn.R

class CustomAdapterHistory(private val dataSet: List<Data>, private val callback: RecyclerCallback.HistoryCallback) :
    RecyclerView.Adapter<CustomAdapterHistory.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.orderhistory_row, viewGroup, false)
        )

    override fun getItemCount() = dataSet.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataSet[position],position, callback)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val date: TextView = itemView.findViewById(R.id.dateText)
        val status: TextView = itemView.findViewById(R.id.statusText)
        val address: TextView = itemView.findViewById(R.id.addressText)
        val price: TextView = itemView.findViewById(R.id.priceText)
        val comments: TextView = itemView.findViewById(R.id.commentsText)

        fun bind(data: Data,position: Int, callback: RecyclerCallback.HistoryCallback) {
            address.text = data.address
            status.text = data.status
            date.text = data.created_at
            price.text = data.charge.toString()
            comments.text = data.comments

            itemView.setOnClickListener {
                callback.onRowClicked(position, it)
            }
        }
    }
}