package com.example.corn.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.corn.activities.OrderActivity
import com.example.corn.R
import com.example.corn.activities.TermsActivity
import com.example.corn.databinding.FragmentDashboardBinding


class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val boiledButton:Button? = root?.findViewById(R.id.boiledButton)
        val roastedButton: Button? =  root?.findViewById(R.id.roastedButton)

        val PREFS_NAME = "MyPrefsFile"

        val sharedPreferences = activity?.getSharedPreferences(PREFS_NAME, 0)
        val api_token = sharedPreferences?.getString("api_token","")
        if (sharedPreferences!!.getBoolean("my_first_time", true)||api_token == "1") {
            //the app is being launched for first time, do something
            //Log.d("Comments", "First time")

            // first time task
            startActivity(Intent(activity, TermsActivity::class.java))

            // record the fact that the app has been started at least once
            sharedPreferences.edit().putBoolean("my_first_time", false).commit()
        }

        if (boiledButton != null) {
            boiledButton.setOnClickListener {

                val intent = Intent(getActivity(), OrderActivity::class.java)

                intent.putExtra ("productID",2)
                getActivity()?.startActivity(intent)
            }
        }
        if (roastedButton != null) {
            roastedButton.setOnClickListener {

                val intent = Intent(getActivity(), OrderActivity::class.java)

                intent.putExtra ("productID",1)
                getActivity()?.startActivity(intent)
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}