package com.example.corn.ui.main

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    // TODO: Implement the ViewModel
    var currentLocation = MutableLiveData<Location>()
    var currentAddress = MutableLiveData<String>()
}