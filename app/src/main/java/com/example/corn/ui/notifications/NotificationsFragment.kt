package com.example.corn.ui.notifications

import android.content.Context
import android.content.Intent
import com.example.corn.Network.OrderHistory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.corn.Network.ApiInterface
import com.example.corn.Network.Data
import com.example.corn.R
import com.example.corn.activities.MapActivity
import com.example.corn.activities.OrderRepeatActivity
import com.example.corn.adapters.CustomAdapter
import com.example.corn.adapters.CustomAdapterHistory
import com.example.corn.adapters.RecyclerCallback
import com.example.corn.databinding.FragmentNotificationsBinding
import com.example.corn.singletons.Cart
import com.example.corn.singletons.DataModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private var _binding: FragmentNotificationsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
                ViewModelProvider(this).get(NotificationsViewModel::class.java)

        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root

       // val textView: TextView = binding.textNotifications
        notificationsViewModel.text.observe(viewLifecycleOwner, Observer {
         //   textView.text = it
        })
        val spinner:ProgressBar = root.findViewById(R.id.progressBar2)
      //  val spinner: ProgressBar? = view?.findViewById(R.id.progressBar2);
        spinner.setVisibility(View.VISIBLE)
        val SHARED_PREF = "sharedPreferences"
        val sharedPreferences = activity?.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
        val api_token = sharedPreferences?.getString("api_token","noapitoken")
        val apiInterface = ApiInterface.create().getOrderHistory(api_token!!)

        //apiInterface.enqueue( Callback<List<Movie>>())
        apiInterface.enqueue(object : Callback<OrderHistory> {
            override fun onResponse(call: Call<OrderHistory>?, response: Response<OrderHistory>?) {
                spinner.setVisibility(View.GONE)
                if (response?.body() != null) {

                    val json = response.body()
                    // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                   println(json.toString())
                    if (json != null) {
                        DataModel.orderHistory = json

                        val recycle_adapter = CustomAdapterHistory(DataModel.orderHistory!!.data,
                            object : RecyclerCallback.HistoryCallback {
                                override fun onRowClicked(position:Int, view: View) {
                                    // ToDo Do onClick
                                    println("Im a row and i got clicked")
                                    val intent = Intent(getActivity(), OrderRepeatActivity::class.java)

                                    intent.putExtra("position",position)
                                    getActivity()?.startActivity(intent)
                                }
                            })
                        // val root = inflater.inflate(R.layout.cart_fragment, container, false)
                        val view: View = root.rootView
                        val recyclerView: RecyclerView = view.findViewById(R.id.historyRecycler)
                        recyclerView.layoutManager =
                            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                        recyclerView.adapter = recycle_adapter

                    }
                }
            }

            override fun onFailure(call: Call<OrderHistory>?, t: Throwable?) {
                spinner.setVisibility(View.GONE)
                Toast.makeText(activity, "something went wrong"+ t, Toast.LENGTH_SHORT).show()
            }
        })

            return root
        }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}