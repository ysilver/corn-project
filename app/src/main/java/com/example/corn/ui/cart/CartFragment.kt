package com.example.corn.ui.cart

import android.annotation.SuppressLint
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.corn.*
import com.example.corn.activities.MapActivity
import com.example.corn.adapters.CustomAdapter
import com.example.corn.singletons.Cart

class CartFragment : Fragment() {

    companion object {
        fun newInstance() = CartFragment()
    }

    private lateinit var viewModel: CartViewModel

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //val grandTotal:EditText? = view?.findViewById(R.id.totalPrice)
        val recycle_adapter = CustomAdapter(Cart.cartItems)
        val root = inflater.inflate(R.layout.cart_fragment, container, false)
        val view:View =root.rootView
        val recyclerView:RecyclerView = view.findViewById(R.id.recycleView)
        recyclerView.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
        recyclerView.adapter = recycle_adapter

        val orderButton: Button? = view.findViewById(R.id.sendOrderButton)
        val grandTotal: TextView? = view.findViewById(R.id.totalPrice)

        orderButton?.setOnClickListener{
            val intent = Intent(getActivity(), MapActivity::class.java)

           // intent.putExtra ("productID",2)
            getActivity()?.startActivity(intent)
        }
        var total:Double = 0.0
        for (price in Cart.cartItems){

            total = total+(price.price.toDouble())
        }
        grandTotal?.setText(total.toBigDecimal().toPlainString())
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CartViewModel::class.java)
        // TODO: Use the ViewModel


    }

}