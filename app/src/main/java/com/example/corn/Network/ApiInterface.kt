package com.example.corn.Network

import com.example.corn.Order
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    @POST("api/orders")
     fun placeOrder(@Body order: Order,@Query(value="api_token",encoded = false)api_token:String): Call<OrderReturn>

     @GET("api/orders")
    fun getOrderHistory(@Query (value="api_token",encoded = false)api_token: String): Call<OrderHistory>

    @POST("api/clients/register")
    fun registerPhone(@Body register: RegisterPhone): Call<RegisterReturn>

    @POST("api/clients/verify/{mobile}")
    fun verifyPhone(@Body verify: VerifyPhone, @Path(value="mobile",encoded = false)mobile:String?): Call<VerifyReturn>
    companion object {

        var BASE_URL = "https://corn-delivery.eu/"

        fun create() : ApiInterface {

            val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }
}