package com.example.corn.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import com.example.corn.singletons.Cart
import com.example.corn.models.CartItem
import com.example.corn.R

class OrderActivity : AppCompatActivity() {
        var productId:Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)
        val itemDesc: EditText? = findViewById(R.id.itemDesc)
        val comments: EditText? = findViewById(R.id.comments)
        val increase : ImageButton? = findViewById(R.id.increase)
        val decrease :ImageButton? = findViewById(R.id.decrease)
        val price : EditText? = findViewById(R.id.price)
         val quantity : EditText? = findViewById(R.id.quantity)
        val checkoutButton:Button? = findViewById(
            R.id.button1
        )
        val continueButton: Button? = findViewById(
            R.id.button2
        )
       val   image:ImageView? = findViewById(R.id.imageView3)
       productId =  intent.getIntExtra("productID",0)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
        quantity?.setText("2")
        price?.setText("3.99")

        increase?.setOnClickListener{
                var qnt = quantity?.text.toString().toInt()
                qnt += 1
                quantity?.setText(qnt.toString())
               setUpPrice(qnt)

        }
        decrease?.setOnClickListener{

                var qnt = quantity?.text.toString().toInt()
                    qnt -= 1
                    if (qnt == 1){
                        price?.setText("")
                    return@setOnClickListener
            }
                    quantity?.setText(qnt.toString())
                   setUpPrice(qnt)

        }


        checkoutButton?.setOnClickListener{
            var imageName = ""
            if (productId == 1){
                imageName = "roasted"
            }
            else{
                imageName = "bolied"
            }
                Cart.cartItems.add(CartItem(productId.toString(),itemDesc?.text.toString(),imageName,quantity?.text.toString(),price?.text.toString(),comments?.text.toString()))
            val intent = Intent(this, CartActivity::class.java)

         //   intent.putExtra ("productID",2)
            this?.startActivity(intent)

        }

        continueButton?.setOnClickListener{
            var imageName = ""
            if (productId == 1){
                imageName = "roasted"
            }
            else{
                imageName = "bolied"
            }
            Cart.cartItems.add(CartItem(productId.toString(),itemDesc?.text.toString(),imageName,quantity?.text.toString(),price?.text.toString(),comments?.text.toString()))

            finish()
        }

        if  (productId == 1) {
            image?.setBackgroundResource(R.drawable.roasted)
            itemDesc?.setText("Roasted Corn")
        }
        else{
            image?.setBackgroundResource(R.drawable.bolied)
            itemDesc?.setText("Boiled Corn")
        }

    }

    fun setUpPrice(quantity:Int){
        val price:EditText? = findViewById(R.id.price)
        if (quantity == 2){
               price?.setText("3.99")
        }
        else if (quantity == 3){

            price?.setText("5.99")
        }
        else if (quantity == 4){
            price?.setText("7.99")
        }
        else if( quantity >= 5){
            price?.setText("9.99")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}