package com.example.corn.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.corn.R
import com.example.corn.activities.ui.main.OrderRepeatFragment
import com.example.corn.ui.cart.CartFragment

class OrderRepeatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.order_repeat_layout)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, OrderRepeatFragment.newInstance())
                .commitNow()
        }
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }
}