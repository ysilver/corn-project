package com.example.corn.activities

import android.content.Context
import com.example.corn.Network.OrderReturn
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.corn.*
import com.example.corn.Network.ApiInterface
import com.example.corn.models.CartItem
import com.example.corn.singletons.Cart
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CommentsActivity : AppCompatActivity() {
    var address = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)
        address = intent.getStringExtra("address").toString()
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
        address = intent.getStringExtra("address").toString()
        val comments: TextView? = findViewById(R.id.comments)
        val placeOrderBtn: Button? = findViewById(R.id.placeOrderBtn)

         val spinner:ProgressBar = findViewById(R.id.progressBar1)
        spinner.setVisibility(View.GONE)
        placeOrderBtn?.setOnClickListener {
            spinner.setVisibility(View.VISIBLE)
            var items: MutableList<Item> = arrayListOf()
            for (item in Cart.cartItems) {
                var orderitem = Item(item.productID.toLong(), item.qnt.toLong(), item.comments)
                items?.add(orderitem)
            }
            val order: Order = Order(address, comments?.text.toString(), items)

         //  val apiService = RestApiService()

            val SHARED_PREF = "sharedPreferences"
            val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
            val api_token = sharedPreferences.getString("api_token","noapitoken")
            val apiInterface = ApiInterface.create().placeOrder(order,api_token!!)

            //apiInterface.enqueue( Callback<List<Movie>>())
            apiInterface.enqueue(object : Callback<OrderReturn> {
                override fun onResponse(call: Call<OrderReturn>?, response: Response<OrderReturn>?) {

                    if (response?.body() != null) {
                        spinner.setVisibility(View.GONE)
                        val json = response.body()
                        // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                        System.out.println(json.toString())
                        if (json != null) {
                            if (json.id != 0) {
                                Toast.makeText(
                                    applicationContext,
                                    "Your order was received and its on its way!",
                                    Toast.LENGTH_LONG
                                ).show()
                                Cart.cartItems = ArrayList<CartItem>()
                                startActivity(Intent(applicationContext, MainActivity::class.java))
                                //}

                            }
                        }
                    }
                }
                override fun onFailure(call: Call<OrderReturn>?, t: Throwable?) {
                    spinner.setVisibility(View.GONE)
                    Toast.makeText(application, "something went wrong"+ t, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }
}



