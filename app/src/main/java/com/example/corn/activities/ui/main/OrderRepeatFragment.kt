package com.example.corn.activities.ui.main

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.corn.R
import com.example.corn.activities.MapActivity
import com.example.corn.activities.OrderRepeatActivity
import com.example.corn.adapters.CustomAdapterHistory
import com.example.corn.adapters.CustomAdapterOrderItems
import com.example.corn.adapters.RecyclerCallback
import com.example.corn.models.CartItem
import com.example.corn.singletons.Cart
import com.example.corn.singletons.DataModel

//import com.example.corn.activities.R

class OrderRepeatFragment : Fragment() {

    companion object {
        fun newInstance() = OrderRepeatFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val position = requireActivity().intent.getIntExtra("position", 0)
        val recycle_adapter = CustomAdapterOrderItems(
            DataModel.orderHistory!!.data[position].order_items,
            object : RecyclerCallback.HistoryCallback {
                override fun onRowClicked(position:Int, view: View) {

                }
            })
        val root = inflater.inflate(R.layout.orp_fragment, container, false)
        val view:View =root.rootView
        val recyclerView: RecyclerView = view?.findViewById(R.id.recycleViewOrderItems)
        recyclerView.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recyclerView.adapter = recycle_adapter

        val repeatOrderBtn : Button = view?.findViewById(R.id.repeatOrderBtn)

        repeatOrderBtn.setOnClickListener{
           for (orderItem in DataModel.orderHistory!!.data[position].order_items){
               Cart.cartItems.add(CartItem(orderItem.product_id.toString(),orderItem.name,"bolied",orderItem.quantity.toString(),orderItem.charge.toString(),orderItem.comments))
           }
            val intent = Intent(activity, MapActivity::class.java)

            // intent.putExtra ("productID",2)
            getActivity()?.startActivity(intent)
        }
        return root //inflater.inflate(R.layout.orp_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

}