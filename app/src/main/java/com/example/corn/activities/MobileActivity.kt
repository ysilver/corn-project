package com.example.corn.activities

import com.example.corn.Network.RegisterReturn
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.corn.Network.ApiInterface
import com.example.corn.R
import com.example.corn.Network.RegisterPhone
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile)

        val nextBtn: Button = findViewById(R.id.nextButton)
        val userName :EditText = findViewById(R.id.usernameText)
        val mobile :EditText = findViewById(R.id.mobileNoText)
        nextBtn.setOnClickListener{

            if( userName.text.toString() == "" || mobile.text.toString() == ""){
                Toast.makeText(this, "Username and mobile are required to identify you with the system.",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            var newPhoneRegister = RegisterPhone(userName.text.toString(),mobile.text.toString().toLong())

            val apiInterface = ApiInterface.create().registerPhone(newPhoneRegister)


            apiInterface.enqueue(object : Callback<RegisterReturn> {
                override fun onResponse(call: Call<RegisterReturn>?, response: Response<RegisterReturn>?) {

                    if (response?.body() != null) {
                        val json = response.body()
                        // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                        System.out.println(json.toString())
                        if (json != null) {
                            if (json.message == "Sending SMS verification") {

                                val SHARED_PREF = "sharedPreferences"
                                val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)

                                val username = sharedPreferences.edit()
                                val mob = sharedPreferences.edit()

                                mob.putString("mobile",mobile.text.toString())
                                // setting it as a boolean
                                username.putString("username",userName.text.toString())


                                // committing or applying is the last step to save, without this those data above wont be saved. commit returns a boolean, apply does not
                                username.commit()
                                mob.commit()

                                Toast.makeText(
                                    applicationContext,
                                    "We will send a 4 digit code via sms",
                                    Toast.LENGTH_LONG
                                ).show()
                                //Cart.cartItems = ArrayList<CartItem>()
                                startActivity(Intent(applicationContext, CodeActivity::class.java))
                                //}

                            }
                        }
                    }
                }
                override fun onFailure(call: Call<RegisterReturn>?, t: Throwable?) {
                    Toast.makeText(application, "something went wrong"+ t, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }
}