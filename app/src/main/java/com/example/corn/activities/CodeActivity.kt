package com.example.corn.activities

import com.example.corn.Network.VerifyReturn
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.corn.Network.ApiInterface
import com.example.corn.R
import com.example.corn.Network.VerifyPhone
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code)
        val finishBtn : Button =findViewById(R.id.verifyButton)
        val codeText : EditText = findViewById(R.id.codeText)

        finishBtn.setOnClickListener{

            if (codeText.text.toString() == ""){

                Toast.makeText(this,"You need to provide the code we sent you with sms.",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val SHARED_PREF = "sharedPreferences"
            val sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
            val mobile = sharedPreferences.getString("mobile","nomobile")
            var code = codeText.text.toString()
            var codeParams = VerifyPhone(code)
            val apiInterface = ApiInterface.create().verifyPhone(codeParams,mobile)


            apiInterface.enqueue(object : Callback<VerifyReturn> {
                override fun onResponse(call: Call<VerifyReturn>?, response: Response<VerifyReturn>?) {

                    if (response?.body() != null) {
                        val json = response.body()
                        // val orderReturn = Gson().fromJson(json.toString(),com.example.corn.Network.OrderReturn::class.java)
                        System.out.println(json.toString())
                        if (json != null) {
                           // if (json.phone.toString() == mobile) {
                                val api_key = sharedPreferences.edit()
                                api_key.putString("api_token",json.api_token)
                                api_key.commit()
                                startActivity(Intent(applicationContext, MainActivity::class.java))
                                //}

           //                 }
                        }
                    }
                }
                override fun onFailure(call: Call<VerifyReturn>?, t: Throwable?) {
                    Toast.makeText(application, "something went wrong"+ t, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }
}