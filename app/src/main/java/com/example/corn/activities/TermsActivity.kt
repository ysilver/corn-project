package com.example.corn.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.corn.R

class TermsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms)

        val nextBtn: Button = findViewById(R.id.nextButton)

        nextBtn.setOnClickListener{

            startActivity(Intent(applicationContext, MobileActivity::class.java))

        }
    }
}